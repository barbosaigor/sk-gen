#include <iostream>
#include "skia/include/core/SkPaint.h"
#include "skia/include/core/SkCanvas.h"

int main() 
{
	auto paint = new SkPaint();
	paint->setAntiAlias(false);
	SkCanvas * canvas = nullptr;
	auto a = 10;
	std::cout << a << '\n';
	return 0;
}